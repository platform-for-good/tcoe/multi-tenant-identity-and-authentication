// 'use strict';

// Configure Environment
const configuration = require('./opt/nodejs/node_modules/config.json');

// Declare shared modules
const tokenManager = require('./opt/nodejs/node_modules/token-manager.js');
const userHelper = require('./opt/nodejs/node_modules/user-helperNP.js');

/**
 * Provision a new tenant admin user
 */
exports.handler = (event, context) => {
    var user = JSON.parse(event.body);

    // get the credentials for the system user
    var credentials = {};
    tokenManager.getSystemCredentials(function (systemCredentials) {
        credentials = systemCredentials;
        // provision the tenant admin and roles
        userHelper.provisionAdminUserWithRoles(user, credentials, configuration.userRole.tenantAdmin, configuration.userRole.tenantUser,
            function(err, result) {
                if (err)
                {
                    context.succeed({
                        statusCode:400,
                        body:JSON.stringify({"Error": "Error Creating Tenant Admin"})
                    });
                }
                else
                context.succeed({
                    statusCode:200,
                    body: JSON.stringify(result)
                });
            });
    });
};