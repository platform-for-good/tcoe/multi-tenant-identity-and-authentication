// 'use strict';

// Declare shared modules
const tokenManager = require('./opt/nodejs/node_modules/token-manager.js');
const userHelper = require('./opt/nodejs/node_modules/user-helperNP.js');

/**
 * Lookup user pool for any user - no user data returned
 */
exports.handler = (event, context) => {

    var body = JSON.parse(event.body);
    tokenManager.getSystemCredentials(function (credentials) {
        // body.id is the tenant.userName which is email
        userHelper.lookupUserPoolData(credentials, body.id, null, true, function (err, user) {
            if (err || user === undefined) {
                context.succeed({
                    statusCode: 400,
                    body: JSON.stringify({ error: "Error getting user" })
                });
            }
            else {
                if (user.length == 0)
                    context.succeed({
                        statusCode: 400,
                        body: JSON.stringify({ error: "User not found" })
                    });
                else
                    context.succeed({
                        statusCode: 200,
                        body: JSON.stringify(user)
                    });
            }
        });
    });
};